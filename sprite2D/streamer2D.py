
#!/usr/bin/env python
# encoding: utf-8
r"""
2-D cylindrically symmetric streamer equations
===============================================
"""
import numpy as np
import scipy.constants as co
from scipy import interpolate
import time

import riemann_solver # Riemann solver
import poisson_mg as poisson # Electromagnetic module
import default as chemistry # Chemistry module
from clawpack import riemann

from mpi4py import MPI

# Average molecular weight of air
mair = 28.97 * co.gram / co.N_A

# Number density of air
nair = 2.5e25#co.value('Loschmidt constant (273.15 K, 101.325 kPa)')

# Mass density of air
rhoair = mair * nair

# Useful constants and parameters
epsilon_0 = co.epsilon_0
charge = co.e

# Starts settings class
import settings
settings = settings.settings()
settings()

settings.chem()

# Initiates air chemistry class
air = chemistry.HumidAir(water_content=settings.water_content,folder=settings.folder,vib=settings.vib)

nspecies = air.nspecies
num_eqn = nspecies
dspecies = air.dspecies

# Useful labels
e = dspecies['e']
O2p = dspecies['O2+']
N2p = dspecies['N2+']

# Sign of the electric charge
pos = [s for s in dspecies if '+' in s]
neg = [s for s in dspecies if '-' in s]
charged_pos = {}
for species in pos:
    charged_pos[species] = 1
charged_neg = {}
for species in neg:
    charged_neg[species] = -1
charged_neg['e'] = - 1

charged_species = dict(charged_neg,**charged_pos)
charge_sign = air.species_vector(charged_species)


# Space parameters
dimx = settings.mx
dimy = settings.my
az = settings.az
ar = settings.ar
bz = settings.bz
br = settings.br
dz = (bz - az)/dimx
dr = (br - ar)/dimy


class density():

    def __init__(self,Nair_centre=nair,Nair_edge=nair,z=None,ze=None):
        self.Nair_centre = Nair_centre
        self.Nair_edge = Nair_edge
        self.ze = ze
        self.z = z

    def profile(self, H = 7.2e3):
        if settings.constant_density == False:
            if self.ze is not None:
                self.Nair_edge = ninterp_air(self.ze[...,None])
            if self.z is not None:
                self.Nair_centre = ninterp_air(self.z[...,None])

# Starts density class
air_density = density()

# Reads initial conditions
folder_init = './Initial_conditions/'
height, nCO2 = np.loadtxt(folder_init + 'CO2.dat',unpack=True)
height = height[1:]*1e3
nCO2 = nCO2[1:]/co.centi**3
ninterp_CO2 = interpolate.interp1d(height,nCO2)

# Initialized with data at 80 km
dataN2v = np.loadtxt(folder_init + 'N2v.dat')
dataN2v = dataN2v[1]

x_N2v1 = dataN2v[1]
x_N2v2 = dataN2v[2]
x_N2v3 = dataN2v[3]
x_N2v4 = dataN2v[4]
x_N2v5 = dataN2v[5]
x_N2v6 = dataN2v[6]
x_N2v7 = dataN2v[7]
x_N2v8 = dataN2v[8]
x_N2v9 = dataN2v[9]
x_N2v10 = dataN2v[10]

data = np.loadtxt(folder_init + 'waccm_fg_l38.dat',comments='!')
data = np.flip(data,axis=0)
height1 = data[:,0]*1e3
data[:,3:] /= co.centi**3
n_air = data[:,3]
n_O = data[:,4]
n_O3 = data[:,5]
n_N = data[:,6]
n_NO = data[:,7]
n_NO2 = data[:,8]
n_NO3 = data[:,9]
n_N2O = data[:,10]
n_N2O5 = data[:,11]
n_CO = data[:,18]
ninterp_O = interpolate.interp1d(height1,n_O)
ninterp_O3 = interpolate.interp1d(height1,n_O3)
ninterp_N = interpolate.interp1d(height1,n_N)
ninterp_NO = interpolate.interp1d(height1,n_NO)
ninterp_NO2 = interpolate.interp1d(height1,n_NO2)
ninterp_NO3 = interpolate.interp1d(height1,n_NO3)
ninterp_N2O = interpolate.interp1d(height1,n_N2O)
ninterp_N2O5 = interpolate.interp1d(height1,n_N2O5)
ninterp_CO = interpolate.interp1d(height1,n_CO)
ninterp_air = interpolate.interp1d(height1,n_air)

def qinit(state):

    grid = state.grid
    z, r = grid.p_centers

    state.q[:] = 0
    # Neutral species
    state.q[dspecies['CO2']] = ninterp_CO2(z)
    state.q[dspecies['O']] = ninterp_O(z)
    state.q[dspecies['O3']] = ninterp_O3(z)
    state.q[dspecies['N']] = ninterp_N(z)
    state.q[dspecies['NO']] = ninterp_NO(z)
    state.q[dspecies['NO2']] = ninterp_NO2(z)
    state.q[dspecies['NO3']] = ninterp_NO3(z)
    state.q[dspecies['N2O']] = ninterp_N2O(z)
    state.q[dspecies['CO']] = ninterp_CO(z)

    air_density.z = z[:,0]
    air_density.profile()
    Nair = air_density.Nair_centre
    state.q[dspecies['O2']] = 0.21 * ninterp_air(z)
    state.q[dspecies['N2']] = 0.79 * ninterp_air(z)
    state.q[dspecies['M']] = ninterp_air(z)

    state.q[dspecies['N2v1']] = x_N2v1 * state.q[dspecies['N2']]
    state.q[dspecies['N2v2']] = x_N2v2 * state.q[dspecies['N2']]
    state.q[dspecies['N2v3']] = x_N2v3 * state.q[dspecies['N2']]
    state.q[dspecies['N2v4']] = x_N2v4 * state.q[dspecies['N2']]
    state.q[dspecies['N2v5']] = x_N2v5 * state.q[dspecies['N2']]
    state.q[dspecies['N2v6']] = x_N2v6 * state.q[dspecies['N2']]
    state.q[dspecies['N2v7']] = x_N2v7 * state.q[dspecies['N2']]
    state.q[dspecies['N2v8']] = x_N2v8 * state.q[dspecies['N2']]
    state.q[dspecies['N2v9']] = x_N2v9 * state.q[dspecies['N2']]
    state.q[dspecies['N2v10']] = x_N2v10 * state.q[dspecies['N2']]

    # Charged species
    # Background electron density
    ne0 = 1.e4
    L = 2.86e3
    state.q[e] = ne0*np.exp((z-60.e3)/L)

    # Seed streamer inception (neutral)
    sigmas = 40.
    ne0 = 5e10
    state.q[e] += ne0*np.exp( -(z-bz)**2/sigmas**2
                                - r**2 /sigmas**2)
    state.q[O2p] = 0.21 * state.q[e] # N_O2+
    state.q[N2p] = 0.79 * state.q[e] # N_N2+


def auxinit(state):
    """
    aux[0,i,j] = radial coordinate of cell centers for cylindrical source terms
    """
    y = state.grid.y.centers
    for j, r in enumerate(y):
        state.aux[0, :, j] = r

    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * state.q[:] * charge, axis=0)
    rhs = - dens/epsilon_0
    state.aux[-2] = dens

    # Potential (Domain decomposition)
    state.problem_data['IG'] = None
    state.problem_data['IGDM'] = None
    state.problem_data['IGDM1'] = None

    edges = True # To perform the calculation of the electric field at the edges
    Ez,Er,Eze,Ere = poisson.efield(-rhs, state, edges)

    # Background electric field
    Ez += state.problem_data['Ebk']
    # Electric field at the cell edges
    Eze +=  state.problem_data['Ebk'] # E_{i-1/2,j}

    z, r = state.grid.x.centers,state.grid.y.centers
    ze,re = state.grid.x.nodes, state.grid.y.nodes

    Nair_centre = air_density.Nair_centre
    air_density.ze = ze
    air_density.profile()
    Nair_edge = air_density.Nair_edge

    E_interp = interp(state.grid.x.centers,state.grid.y.centers,1.e21*np.sqrt(Ez**2 + Er**2)/Nair_centre)

    E_lr = E_interp(ze,r)
    E_bt = E_interp(z,re)
    # Initialize aux vecs devoted to the electric field and drift velocities
    mu_e_lr = charge_sign[dspecies['e']]*settings.mu_e(E_lr)/Nair_edge
    mu_e_bt = charge_sign[dspecies['e']]*settings.mu_e(E_bt)/Nair_centre
    state.aux[1] = Ez
    state.aux[2] = Er
    state.aux[3] = mu_e_lr[:-1] * Eze[:-1]
    state.aux[4] = mu_e_bt[:,:-1] * Ere[:,:-1]

    state.problem_data['vze'] = mu_e_lr[-1] * Eze[-1]
    state.problem_data['vre'] = mu_e_bt[:,-1] * Ere[:,-1]


    state.problem_data['dN_dt'] = None
    state.problem_data['derived'] = 0.

    # Dictionary to manage aux writing
    dict_aux = {}
    dict_aux['write'] = True
    dict_aux['num_aux_tbw'] = 4
    dict_aux['list'] = np.array((1,2,5,6))
    state.problem_data['set_aux_written'] = dict_aux

    # For photoionization
    state.problem_data['ph1'] = None
    state.problem_data['ph1DM'] = None
    state.problem_data['ph2'] = None
    state.problem_data['ph2DM'] = None
    state.problem_data['ph3'] = None
    state.problem_data['ph3DM'] = None


def b4step2(solver,state):
    state.q[:] = np.maximum(0.,state.q[:])

    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * state.q[:] * charge, axis=0)
    rhs = - dens/epsilon_0
    state.aux[-2] = dens

    # Potential computation (Domain decomposition)
    edges = True
    Ez,Er,Eze,Ere  = poisson.efield(-rhs, state, edges)

    # Background electric field
    Ez += state.problem_data['Ebk']
    # Electric field at the cell edges
    Eze += state.problem_data['Ebk'] # E_{i-1/2,j}, i= 0.., j = 0...

    z, r = state.grid.x.centers,state.grid.y.centers
    ze,re = state.grid.x.nodes, state.grid.y.nodes
    Nair_centre = air_density.Nair_centre
    Nair_edge = air_density.Nair_edge
    E_interp = interp(state.grid.x.centers,state.grid.y.centers,1.e21*np.sqrt(Ez**2 + Er**2)/Nair_centre)

    E_lr = E_interp(ze,r)
    E_bt = E_interp(z,re)
    # Initialize aux vecs devoted to the electric field and drift velocities
    mu_e_lr = charge_sign[dspecies['e']]*settings.mu_e(E_lr)/Nair_edge
    mu_e_bt = charge_sign[dspecies['e']]*settings.mu_e(E_bt)/Nair_centre

    state.aux[1] = Ez
    state.aux[2] = Er
    state.aux[3] = mu_e_lr[:-1] * Eze[:-1]
    state.aux[4] = mu_e_bt[:,:-1] * Ere[:,:-1]

    state.problem_data['vze'] = mu_e_lr[-1] * Eze[-1]
    state.problem_data['vre'] = mu_e_bt[:,-1] * Ere[:,-1]


def dq_cEuler_radial(solver, state, dt):
    """
    This is a Clawpack-style source term routine, which approximates
    the integral of the source terms over a step.
    """

    q = state.q
    aux = state.aux
    Ez = aux[1]
    Er = aux[2]

    dq = dq_comp(state, q, Ez, Er, dt/2.)

    qstar = np.empty(q.shape)
    qstar = q + dq

    ################Second order ##################################
    # Compute charge density and call Poisson equation solver
    dens = np.sum(charge_sign[...,None,None] * qstar * charge, axis=0)

    rhs = - dens/epsilon_0

    # Electric field (DDM)
    Ez,Er = poisson.efield(-rhs, state)
    Ez += state.problem_data['Ebk']

    dq = dq_comp(state, qstar, Ez, Er, dt)

    # In clawpack-style solvers the source terms update q.
    q += dq


def dq_comp(state, q, Ez, Er, dt):

    rad = state.aux[0]

    # Reduced electric field
    Nair = air_density.Nair_centre
    Emod = np.sqrt(Ez**2 + Er**2)
    Ered = (Emod/Nair) * (1e21) # Townsend

    vdrifty = charge_sign[dspecies['e']]*(settings.mu_e(Ered)/Nair) * Er

    # Vector encoding dn/dt for each species
    if (0.<state.t):
       super_dn_dt = chemistry.chemical(air,q,Ered) # dn/dt
    else:
       super_dn_dt = np.zeros(state.q.shape)

    qbcstar = get_qbcstar_from_qstar(state,q)
    dq = np.zeros(q.shape)
    dq[e] = -dt/rad * (q[e]*vdrifty) + super_dn_dt[e] * dt + diff(state,qbcstar[e],Ered) * dt
    dq[1:] = super_dn_dt[1:]*dt

    if (state.problem_data['photo'] is True) and (0.<state.t):

        Sph = np.maximum(poisson.photo(super_dn_dt[dspecies['V']],state),0.)

        dq[e] += Sph * dt
        dq[O2p] += Sph * dt

    # Save dNe_dt
    if (0.<state.t):
       state.problem_data['dN_dt'] = np.abs(super_dn_dt)


    return dq


def diff(state,qbc,en):

    re = state.grid.y.nodes
    r = state.aux[0]

    z, r = state.grid.x.centers,state.grid.y.centers
    ze,re = state.grid.x.nodes, state.grid.y.nodes
    Nair_centre = air_density.Nair_centre
    Nair_edge = air_density.Nair_edge
    E_interp = interp(state.grid.x.centers,state.grid.y.centers,en)

    E_lr = E_interp(ze,r)
    E_bt = E_interp(z,re)

    De_lr = settings.D_e(E_lr)/air_density.Nair_edge
    De_bt = settings.D_e(E_bt)/air_density.Nair_centre


    # Built upon two ghost cells
    if state.grid.x.nodes[-1] == bz:
        qbc[-1,2:-2] = qbc[-3,2:-2]
        qbc[-2,2:-2] = qbc[-3,2:-2]

    if state.grid.x.nodes[0] == az:
        qbc[0,2:-2] = qbc[2,2:-2]
        qbc[1,2:-2] = qbc[2,2:-2]

    if state.grid.y.nodes[-1] == br:
        qbc[2:-2,-1] = qbc[2:-2,-3]
        qbc[2:-2,-2] = qbc[2:-2,-3]

    if state.grid.y.nodes[0] == ar:
        qbc[2:-2,0] = qbc[2:-2,2]
        qbc[2:-2,1] = qbc[2:-2,2]

    # This derivatives has been built upon two ghost cells
    dn_dr_r = re[None,1:] * De_bt[:,1:] * (qbc[2:-2,3:-1] - qbc[2:-2,2:-2])/dr**2
    dn_dr_l = re[None,:-1] * De_bt[:,:-1] * (qbc[2:-2,2:-2] - qbc[2:-2,1:-3])/dr**2
    d2n_dr2 = (dn_dr_r - dn_dr_l)/r

    dn_dz_r = De_lr[1:] * (qbc[3:-1,2:-2] - qbc[2:-2,2:-2])/dz**2
    dn_dz_l = De_lr[:-1] * (qbc[2:-2,2:-2] - qbc[1:-3,2:-2])/dz**2
    d2n_dz2 = dn_dz_r - dn_dz_l

    f = d2n_dr2 + d2n_dz2


    return f


def interp(x,y,z):
    return interpolate.RectBivariateSpline(x,y,z)


def lower_radial_bc(state,dim,t,qbc,auxbc,num_ghost):

    # Mirror
    for i in range(num_ghost):
        qbc[:,:,i] = qbc[:,:,num_ghost + 1 -i]


def lower_radial_auxbc(state,dim,t,qbc,auxbc,num_ghost):

    # Mirror
    for i in range(num_ghost):
        auxbc[:,:,i] = auxbc[:,:,num_ghost + 1 -i]
        auxbc[2,:,i] = - auxbc[2,:,num_ghost +1 - i]
        auxbc[4,:,i] = - auxbc[4,:,num_ghost +1 - i]


def upper_auxbc(state,dim,t,qbc,auxbc,num_ghost):

    # Extrapolation
    for i in range(num_ghost):
        auxbc[:,-num_ghost + i] = auxbc[:,-num_ghost - 1]
        auxbc[3,-num_ghost + i,num_ghost:-num_ghost] = state.problem_data['vze']
        auxbc[:,:,-num_ghost + i] = auxbc[:,:,-num_ghost - 1]
        auxbc[4,num_ghost:-num_ghost,-num_ghost + i] = state.problem_data['vre']


def set_time_step(solver,solution):

    state = solution.state

    cfl = solver.cfl.get_cached_max()
    dt_clawpack = min(solver.dt_max,solver.dt * solver.cfl_desired / cfl)

    E = np.sqrt(state.aux[1]**2 + state.aux[2]**2)
    Nair = air_density.Nair_centre
    sigma = charge * (settings.mu_e(1.e21*E/Nair)/Nair) * state.q[e]
    t_maxwell = co.epsilon_0/sigma
    t_maxwell = t_maxwell[np.isfinite(t_maxwell)]
    t_maxwell = t_maxwell[np.nonzero(t_maxwell)]
    t_maxwell = np.nanmin(t_maxwell)

    dN_dt = state.problem_data['dN_dt']


    if dN_dt is None: t_chemical = 3e-08

    else:
        t_chemical = np.nanmin(np.maximum(np.abs(state.q[:]),1.e6)/np.maximum(np.abs(dN_dt),1e12))

    mintime = min(t_chemical,t_maxwell)
    mintimes = poisson.scattertoall(mintime,MPI.COMM_WORLD)
    mintime = np.amin(mintimes)

    solver.dt = min(dt_clawpack,solver.dt_max,mintime/2.)


    # Computing min. chemical and Maxwell time for the output file
    t_chemical = poisson.scattertoall(t_chemical,MPI.COMM_WORLD)
    t_maxwell = poisson.scattertoall(t_maxwell,MPI.COMM_WORLD)
    t_chemical = np.amin(t_chemical)
    t_maxwell = np.amin(t_maxwell)
    if MPI.COMM_WORLD.Get_rank()==0:
        filename = '_output/dt.dat'
        data = (state.t,t_chemical,t_maxwell,dt_clawpack,solver.dt,cfl)
        with open(filename,"a") as f:
            f.write( str(data)[1:-1] + "\n")


def split(b,ranges):

    a = poisson.tozero(ranges,MPI.COMM_WORLD)
    if MPI.COMM_WORLD.Get_rank()==0:
        a = np.array(a,dtype='i').reshape(MPI.COMM_WORLD.Get_size(),4)
        c = [b[:,xs:xe,ys:ye] for (xs,xe,ys,ye) in a]
    else: c = None

    q = MPI.COMM_WORLD.scatter(c,root=0)


    return q


def get_qbcstar_from_qstar(state,q):
    num_ghost = 2
    shape = [n + 2*num_ghost for n in state.grid.num_cells]

    da = state.q_da
    qglobal = da.createGlobalVector()
    qlocal = da.createLocalVector()
    qglobal_array = da.getVecArray(qglobal)

    qglobal_array[:,:] = np.moveaxis(q,0,2)
    da.globalToLocal(qglobal,qlocal)
    shape.insert(0,state.num_eqn)

    return qlocal.getArray().reshape(shape,order='F').copy()


def setup(use_petsc=False, solver_type='classic', outdir='_output',
          kernel_language='Fortran', disable_output=False, mx=dimx, my=dimy, tfinal=3e-3,
          num_output_times = 300, htmlplot=False, tfluct_solver=False):

    if use_petsc:
        import clawpack.petclaw as pyclaw
    else:
        from clawpack import pyclaw

    # Solver settings
    solver = pyclaw.ClawSolver2D(riemann_solver)
    solver.step_source = dq_cEuler_radial
    solver.source_split = 1
    solver.limiters = 2
    solver.cfl_max = 5.e-1
    solver.cfl_desired = 1.e-1
    solver.before_step = b4step2
    solver.dimensional_split = True
    solver.order = 2
    solver.dt_variable = True
    solver.dt_initial = 1.e-13
    solver.get_dt_new = set_time_step
    solver.dt_max = 3e-8
    solver.verbosity = 0

    solver.user_aux_bc_lower = lower_radial_auxbc
    solver.user_bc_lower = lower_radial_bc
    solver.user_aux_bc_upper = upper_auxbc

    solver.bc_lower[0]=pyclaw.BC.extrap
    solver.bc_upper[0]=pyclaw.BC.extrap
    solver.bc_lower[1]=pyclaw.BC.custom
    solver.bc_upper[1]=pyclaw.BC.extrap
    #Aux variable in ghost cells doesn't matter
    solver.aux_bc_lower[0]=pyclaw.BC.extrap
    solver.aux_bc_upper[0]=pyclaw.BC.custom
    solver.aux_bc_lower[1]= pyclaw.BC.custom
    solver.aux_bc_upper[1]=pyclaw.BC.custom

    claw = pyclaw.Controller()
    claw.solver = solver

    # restart options
    restart_from_frame = None


    if restart_from_frame is None:

        x = pyclaw.Dimension(az, bz, mx, name='x')
        y = pyclaw.Dimension(ar, br, my, name='y')
        domain = pyclaw.Domain([x, y])

        solver.num_waves = 1
        solver.num_eqn = num_eqn
        solver.max_steps = 100000
        num_aux = 2 + 3 + 1 + 1

        state = pyclaw.State(domain, num_eqn, num_aux)
        if MPI.COMM_WORLD.Get_rank()==0:
            filename = '_output/dt.dat'
            data = "Time " + " dt_chemical " + " dt_maxwell " + " dt_clawpack " + " dt " + " cfl "
            with open(filename,"a") as f:
                f.write( data + "\n")
        settings.get_args(state,air)
        settings.mu_e = settings.interp_function(settings.mobility_data)
        settings.D_e = settings.interp_function(settings.diffusion_data)
        qinit(state)
        auxinit(state)
        claw.solution = pyclaw.Solution(state,domain)
        claw.num_output_times = num_output_times
    else:
        solver.num_waves = 1
        solver.num_eqn = num_eqn
        solver.max_steps = 100000
        claw.solution = pyclaw.Solution(restart_from_frame,file_format='hdf5',read_aux=False)
        auxinit(claw.solution.state)
        claw.start_frame = restart_from_frame
        claw.num_output_times = num_output_times - restart_from_frame

    claw.keep_copy = False
    if disable_output:
        claw.output_format = None
    else: claw.output_format = 'hdf5'
    #claw.output_options['chunks'] = True
    claw.tfinal = tfinal
    claw.outdir = outdir
    claw.write_aux_init = True
    claw.write_aux_always = True


    return claw



if __name__=="__main__":
    from clawpack.pyclaw.util import run_app_from_main
    output = run_app_from_main(setup)
